"""
Provides a mixin class for permission handling.
Must be used together with View or a child of View
"""

from django.core.exceptions import PermissionDenied
from django.views.generic import View


class AuthorizationMixin(object):
    permission_classes = []

    def allowed_methods(self):
        """
        Wrap Django's private `_allowed_methods` interface in a public property.
        """
        return self._allowed_methods()

    def get_authenticators(self):
        """
        Instantiates and returns the list of authenticators that this view can use.
        """
        return [auth() for auth in self.authentication_classes]

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        return [permission() for permission in self.permission_classes]

    def check_permissions(self, request):
        """
        Check if the request should be permitted.
        Raises an appropriate exception if the request is not permitted.
        """
        for permission in self.get_permissions():
            if not permission.has_permission(request, self):
                raise PermissionDenied

    def check_object_permissions(self, request, obj):
        """
        Check if the request should be permitted for a given object.
        Raises an appropriate exception if the request is not permitted.
        """
        for permission in self.get_permissions():
            if not permission.has_object_permission(request, self, obj):
                raise PermissionDenied

    def get_object(self, queryset, request=None):
        """
        Return a single object after checking permissions.
        """
        if request is None:
            request = self.request

        self.check_permissions(request)
        self.check_object_permissions(request, queryset)

        return queryset

    def dispatch(self, request, *args, **kwargs):
        self.check_permissions(request)
        return super(AuthorizationMixin, self).dispatch(request, *args, **kwargs)

