from django.views.generic import View
from django.http import HttpResponse

from authorization.views import AuthorizationMixin
from testapp import permissions as perms
from .models import DummyObject

from authorization import permissions


class GenericViewAllowOnlyOwner(AuthorizationMixin, View):
    permission_classes = (permissions.IsAuthenticated, perms.IsDummyObjectOwnerOrDeny, )

    def get(self, request, *args, **kwargs):
        self.check_permissions(request)
        obj = DummyObject.objects.get(id=kwargs['id'])
        self.check_object_permissions(request, obj)
        return HttpResponse()


class GenericViewAllowAnyAuthenticated(AuthorizationMixin, View):
    permission_classes = (permissions.IsAuthenticated, )

    def get(self, request, *args, **kwargs):
        self.check_permissions(request)
        return HttpResponse()


class GenericViewDjangoModelPermissions(AuthorizationMixin, View):
    permission_classes = (permissions.DjangoModelPermissions, )
    queryset = DummyObject.objects.none()

    def get(self, request, *args, **kwargs):
        self.check_permissions(request)
        return HttpResponse()


class TesGetObjectViewParams(AuthorizationMixin, View):
    permission_classes = (permissions.IsAuthenticated, perms.IsDummyObjectOwnerOrDeny, )

    def get(self, request, *args, **kwargs):
        self.get_object(DummyObject.objects.get(id=kwargs['id']), request)
        return HttpResponse()


class TesGetObjectViewNoParams(AuthorizationMixin, View):
    permission_classes = (permissions.IsAuthenticated, perms.IsDummyObjectOwnerOrDeny, )

    def get(self, request, *args, **kwargs):
        self.get_object(DummyObject.objects.get(id=kwargs['id']))
        return HttpResponse()

