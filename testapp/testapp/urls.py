"""django_authorization URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^view1/(?P<id>[0-9])/$', views.GenericViewAllowOnlyOwner.as_view(), name='generic_owner_only'),
    url(r'^view2/(?P<id>[0-9])/$', views.GenericViewAllowAnyAuthenticated.as_view(), name='generic_authenticated_only'),
    url(r'^view3/(?P<id>[0-9])/$', views.GenericViewDjangoModelPermissions.as_view(), name='generic_model_perms_only'),
    url(r'^view4/(?P<id>[0-9])/$', views.TesGetObjectViewParams.as_view(), name='get_object_params'),
    url(r'^view5/(?P<id>[0-9])/$', views.TesGetObjectViewNoParams.as_view(), name='get_object_no_params'),
]
