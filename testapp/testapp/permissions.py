from authorization import permissions


class IsDummyObjectOwnerOrDeny(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user

