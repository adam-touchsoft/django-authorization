from django.db import models
from django.contrib.auth.models import User


class DummyObject(models.Model):
    user = models.ForeignKey(User)

    class Meta:
        permissions = (("view_dummyobject", "Can view dummy object"),)