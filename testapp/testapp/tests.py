from django.test import TestCase
from django.contrib.auth.models import User, Permission


from django.core.urlresolvers import reverse


# Create your tests here.
from .models import DummyObject


class TestMixin(object):
    def setUp(self):
        self.user_1 = User()
        self.user_1.username = 'user1'
        self.user_1.set_password('123456')
        self.user_1.save()

        self.user_2 = User()
        self.user_2.username = 'user2'
        self.user_2.set_password('123456')
        self.user_2.save()

        self.user_3 = User()
        self.user_3.username = 'user3'
        self.user_3.set_password('123456')
        self.user_3.save()
        permission = Permission.objects.get(codename='view_dummyobject')
        self.user_3.user_permissions.add(permission)

        self.object = DummyObject()
        self.object.user = self.user_1
        self.object.save()


class TestAuthenticatedGenericView(TestMixin, TestCase):

    def test_permissions_success(self):
        self.client.login(username='user1', password='123456')
        r = self.client.get(reverse('generic_authenticated_only', args=[self.object.id]))
        self.assertEqual(r.status_code, 200)

    def test_permissions_deny(self):
        r = self.client.get(reverse('generic_authenticated_only', args=[self.object.id]))
        self.assertEqual(r.status_code, 403)


class TestObjectPermissionsGenericView(TestMixin, TestCase):

    def test_object_permissions_success(self):
        self.client.login(username='user1', password='123456')
        r = self.client.get(reverse('generic_owner_only', args=[self.object.id]))
        self.assertEqual(r.status_code, 200)

    def test_object_permissions_deny(self):
        self.client.login(username='user2', password='123456')
        r = self.client.get(reverse('generic_owner_only', args=[self.object.id]))
        self.assertEqual(r.status_code, 403)


class TestModelPermissionsGenericView(TestMixin, TestCase):

    def test_permissions_success(self):
        self.client.login(username='user3', password='123456')
        r = self.client.get(reverse('generic_model_perms_only', args=[self.object.id]))
        self.assertEqual(r.status_code, 200)

    def test_permissions_deny(self):
        self.client.login(username='user1', password='123456')
        r = self.client.get(reverse('generic_model_perms_only', args=[self.object.id]))
        self.assertEqual(r.status_code, 403)


class TestGetObjectParams(TestMixin, TestCase):

    def test_permissions_success(self):
        self.client.login(username='user1', password='123456')
        r = self.client.get(reverse('get_object_params', args=[self.object.id]))
        self.assertEqual(r.status_code, 200)

    def test_permissions_deny(self):
        self.client.login(username='user2', password='123456')
        r = self.client.get(reverse('get_object_params', args=[self.object.id]))
        self.assertEqual(r.status_code, 403)
        
        
class TestGetObjectNoParams(TestMixin, TestCase):

    def test_permissions_success(self):
        self.client.login(username='user1', password='123456')
        r = self.client.get(reverse('get_object_no_params', args=[self.object.id]))
        self.assertEqual(r.status_code, 200)

    def test_permissions_deny(self):
        self.client.login(username='user2', password='123456')
        r = self.client.get(reverse('get_object_no_params', args=[self.object.id]))
        self.assertEqual(r.status_code, 403)
